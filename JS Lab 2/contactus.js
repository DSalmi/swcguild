function requiredFields() {
	var nameRequired = document.getElementById("reqName").value;
	var emailRequired = document.getElementById("emailReq").value;
	var phoneRequired = document.getElementById("phoneReq").value;
	var reasonRequired = document.getElementById("reqReason").value;
	var additionalInfoRequired = document.getElementById("reqAddInfo").value;
	var monReq = document.getElementById("mon").checked;
	var tueReq = document.getElementById("tue").checked;
	var wedReq = document.getElementById("wed").checked;
	var thuReq = document.getElementById("thu").checked;
	var friReq = document.getElementById("fri").checked;
	if (nameRequired == "") {
		alert("Please enter your name!");
	} else if(emailRequired == "" && phoneRequired == "") {
		alert("Please enter some form of contact information!")
	} else if (reasonRequired == "other" || additionalInfoRequired == "") {
		alert("Please enter a reason for inquiry!");
	} else if (monReq == false && tueReq == false && wedReq == false && thuReq == false && friReq == false) {
		alert("Please enter at least one day we can contact you!");
	}
}